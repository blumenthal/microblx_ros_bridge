VMicroblx ROS Bridge
=========
> Collection of interactive and Computational micro function blocks for microblx framework and its integration with ROS framework

Dependencies
-----------

It requires ROS (roscpp library)

Tested on:
* Fuerte

Installation
--------------
* First, be sure to have ROS installed and set up properly its environment variables (ROS_ROOT,ROS_WORKSPACE,...)

* Second, set up the MICROBLX_DIR environment variable
```sh
export MICROBLX_DIR=[path-to-microblx]
```
* Then, download the source in your favorite directory by typing
```sh
git clone [git-repo-url] dillinger
mkdir build && cd build
cmake ..
make
```

Quickstart
-----------
TODO

Design
--------
TODO

License
---
GPLv2

Author
-----
Enea Scioni
