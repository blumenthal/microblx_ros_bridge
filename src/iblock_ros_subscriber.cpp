/*
 * ROS Bridge iblock - Publisher
 * Author: Enea Scioni
 * email: enea dot scioni at unife dot it
 *        enea dot scioni at kuleuven dot be
 */

#include <ros/ros.h>
#include <std_msgs/String.h>
#include "ubx.h"

struct subscriber_data {
  std_msgs::String* ros_msg;
  ros::NodeHandle* nh;
  ros::Subscriber* sub;
  ros::AsyncSpinner* spinner;
  subscriber_data()
    : ros_msg(new std_msgs::String())
  {
  }
};

char subscriber_meta[] =
  "{ doc='ROS bridge subscriber interaction block',"
  "  license='MIT',"
  "  real-time=false,"
  "}";
  
ubx_config_t subscriber_config[] = {
        { .name="topic_name", .type_name = "char" },
        { NULL },
};

ubx_port_t subscriber_ports[] = {
        //{ .name="foo", .attrs=PORT_DIR_IN, .in_type_name="unsigned int" },
//         { .name="bar", .attrs=PORT_DIR_OUT, .out_type_name="unsigned int" },
        { NULL },
};

static int subscribe_fnc(ubx_block_t *i, ubx_data_t* data) 
{
//   struct publisher_info *inf=(publisher_info *)i->private_data;
  ERR("fff");
//   inf->ros_msg->data ="hello from..." + inf->pub->getTopic();
//   std_msgs::String msg;
//   msg.data = "greetings from " + inf->pub->getTopic();
//   ERR("fff");
//   inf->pub->publish(msg);
//   ERR("fff");
  return 0;
}

void subscriber_cb(const std_msgs::String::ConstPtr& msg)
{
  ERR("I received something!!! :-)");
//   subscribe_fnc();
}

extern "C" {
  bool ros_master_check() {
    return ros::master::check();
  }
}

static int subscriber_init(ubx_block_t *b)
{
  int ret=-1;
  if(!ros::isInitialized())
  {
    ERR("ros init");
    int argc=0;
    char ** argv=NULL;
    ros::init(argc,argv,"microblx",ros::init_options::AnonymousName);
    if(ros_master_check())
      ros::start();
    else
    {
      ERR("'roscore' is not running: no ROS functions will be available.");
      ros::shutdown();
      goto out;
    }
  }
  else
    ERR("skip");
  
//   static ros::AsyncSpinner spinner(1); // Use 1 threads
//   spinner.start();
  
    
  char* tname; unsigned int len;
  tname = (char *) ubx_config_get_data_ptr(b, "topic_name", &len);
//   bool isok;
  if(strncmp(tname, "", len)==0) 
  {
    ERR("config topic_name unset");
    goto out;
  }
  
  struct subscriber_data *inf;
  /*allocate internal block data*/
  if((inf=(struct subscriber_data*)calloc(1,sizeof(struct subscriber_data)))==NULL) {
    ERR("failed to allocate subscriber_data");
    goto out;
  }

  b->private_data=inf;

  inf->nh = new ros::NodeHandle();
  inf->spinner = new ros::AsyncSpinner(1);
  inf->sub = new ros::Subscriber(inf->nh->subscribe(tname,10,subscriber_cb));
  inf->spinner->start();

      ret=0;
      goto out;
// out_free:
//       free(b->private_data);
out:
      return ret;
}

static void subscriber_cleanup(ubx_block_t *b) {
  ERR("cleaningup");
  struct subscriber_data* inf = (struct subscriber_data*)(b->private_data);
  inf->sub->shutdown();
  delete inf->sub;
  delete inf->nh;
  free((struct subscriber_data*) b->private_data);
}

/* put everything together */
ubx_block_t subscriber_comp = {
        .name = "rosbridge/subscriber",
        .type = BLOCK_TYPE_INTERACTION,
        .meta_data = subscriber_meta,
        
        .configs = subscriber_config,
        
        /* ops */
        .init = subscriber_init,
        .read=subscribe_fnc,
//         .write=microblx_sendout,
        .cleanup = subscriber_cleanup,
};

static int subscriber_mod_init(ubx_node_info_t* ni)
{
  return ubx_block_register(ni, &subscriber_comp);
}

static void subscriber_mod_cleanup(ubx_node_info_t *ni)
{
  ubx_block_unregister(ni, "rosbridge/subscriber");
}


UBX_MODULE_INIT(subscriber_mod_init)
UBX_MODULE_CLEANUP(subscriber_mod_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)