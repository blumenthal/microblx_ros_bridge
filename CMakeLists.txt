CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
PROJECT(microblx_ros_bridge)

# Compiler and flags required to comile microblx
#SET(CMAKE_CXX_COMPILER /usr/bin/clang++ CACHE PATH "Configure the compiler.")
# or call with: 
# cmake .. -DCMAKE_CXX_COMPILER=/usr/bin/clang++

SET(CMAKE_BUILD_TYPE DEBUG) # optional
SET(CMAKE_CXX_FLAGS "-Wall -Werror -fvisibility=hidden")
SET(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

set (ROS_ROOT $ENV{ROS_ROOT} )
if (ROS_ROOT)
  include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)
endif()

# include(/opt/ros/fuerte/share/ros/core/rosbuild/rosbuild.cmake)
#   rosbuild_init()
#   rosbuild_find_ros_package( roscpp )

# include(/opt/ros/fuerte/share/roscpp/rosbuild/roscpp.cmake)
# set(CMAKE_MODULE_PATH  ${PROJECT_SOURCE_DIR}/cmake-extensions/ )
find_package(PkgConfig)

pkg_check_modules(ROSCPP roscpp)

link_directories(
    ${ROSCPP_LIBRARY_DIRS}
)
include_directories(
    ${ROSCPP_INCLUDE_DIRS}
)

# Common output filders bin and lib
SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_HOME_DIRECTORY}/bin/ CACHE PATH "Configure the executable output path.")
SET(LIBRARY_OUTPUT_PATH ${CMAKE_HOME_DIRECTORY}/lib/ CACHE PATH "Configure the library output path.")
SET( CMAKE_CXX_COMPILER clang++ )
FIND_PACKAGE(Microblx REQUIRED)

# add include directories (-I)
INCLUDE_DIRECTORIES(${UBX_INCLUDE_DIR})
include_directories(/home/haianos/enea_microblx/microblx_cartesiangen/types)
# compile and link the executables
add_library(irospublisher SHARED src/iblock_ros_publisher.cpp) # -fPIC is already included
set_target_properties(irospublisher PROPERTIES PREFIX "")
target_link_libraries(irospublisher ${UBX_LIBRARIES} ${ROSCPP_LIBRARIES})

add_library(irossubscriber SHARED src/iblock_ros_subscriber.cpp)
set_target_properties(irossubscriber PROPERTIES PREFIX "") 
target_link_libraries(irossubscriber ${UBX_LIBRARIES} ${ROSCPP_LIBRARIES})

# add_library(iblock_jnt_test SHARED src/iblock_jnt_test.cpp) # -fPIC is already included
# set_target_properties(iblock_jnt_test PROPERTIES PREFIX "")
# target_link_libraries(iblock_jnt_test ${UBX_LIBRARIES} ${ROSCPP_LIBRARIES})